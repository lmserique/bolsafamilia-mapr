package com.lmserique.bolsafamilia;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class BolsaFamiliaReducer extends Reducer<Text, DoubleWritable, Text, Text>
{

    @Override
    protected void reduce(Text key, Iterable<DoubleWritable> values,
            Reducer<Text, DoubleWritable, Text, Text>.Context context) throws IOException, InterruptedException
    {
        Double valorTotal = 0d;
        for (DoubleWritable value : values)
        {
            valorTotal += value.get();
        }
        
        context.write(key, new Text(String.format("%f",valorTotal)));
    }

}

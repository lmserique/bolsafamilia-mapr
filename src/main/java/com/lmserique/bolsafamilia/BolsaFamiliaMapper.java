package com.lmserique.bolsafamilia;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

public class BolsaFamiliaMapper extends Mapper<LongWritable, Text, Text, DoubleWritable>
{

    private static Logger log = Logger.getLogger(BolsaFamiliaMapper.class);
    
    private Text             chave    = new Text();
    private DoubleWritable   valor    = new DoubleWritable();

    private static final int idxUF    = 2;
    private static final int idxValor = 7;

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, DoubleWritable>.Context context)
            throws IOException, InterruptedException
    {

        
        // "201804";"201801";"AP";"0605";"MACAPA";"16277994485";"GRACINETE GOMES DE FREITAS";"124,00"

        /*
        [0] Ano/Mês Referência:         Ano/Mês da folha de pagamento
        
        [1] Ano/Mês Competência:        Ano/Mês a que se refere a parcela
        
        [2] UF:                         Sigla da Unidade Federativa do beneficiário do Bolsa Família
        
        [3] Código Município SIAFI:     Código, no SIAFI (Sistema Integrado de Administração Financeira), do município do beneficiário do Bolsa Família
        
        [4] Nome Município SIAFI:       Nome do município do beneficiário do Bolsa Família
        
        [5] NIS Beneficiário:           NIS do beneficiário do Bolsa Família.Criado pela Caixa Econômica Federal o NIS significa Número de Identificação 
                                    Social e é ganho quando o cidadão brasileiro ingressa em algum Programa Social, seja o Bolsa Família, FGTS, emitiu 
                                    sua Carteira de Trabalho, tornou-se contribuinte do INSS ou iniciou sua vida como trabalhador de iniciativa privada ou pública.
        
        [6] Nome Beneficiário:          Nome do beneficiário do Bolsa Família
        
        [7] Valor Parcela:              Valor da parcela do benefício 
        
        */

        String campos[] = value.toString().split(";");
        String UF = campos[idxUF].replaceAll("\"", "");

        if ("UF".equals(UF))
        {
            return;
        }
        chave.set(UF);
        valor.set(Double.parseDouble(campos[idxValor].replaceAll(",", ".").replaceAll("\"", "")));
        
        context.write(chave, valor);
    }

}
